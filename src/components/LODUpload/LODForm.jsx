import React, { useState, useContext } from "react";
import { useHistory } from "react-router-dom";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import LODContext from "../../context/LODContext";

function LODForm(props) {
  const history = useHistory();
  const { setStatuses } = useContext(LODContext);

  const [payee, setPayee] = useState("");
  const [artist, setArtist] = useState("");
  const [lodForm, setLodForm] = useState("");
  const [repSheet, setRepSheet] = useState("");
  const [ampCert, setAmpCert] = useState("");

  const options = [
    { value: "", text: "Please select an option" },
    { value: "SX1101FW98", text: "1400 Entertainment Inc." },
    { value: "SX1100DTRG", text: "Adam Anders" },
    { value: "SX101FWI71", text: "Adam Levine" },
    { value: "SX1019JQBI", text: "Andre 3000, Inc.", disabled: true }
  ];

  const handleSubmit = evt => {
    evt.preventDefault();
    let new_status = [];
    if (payee) {
      history.push("/home");
      new_status.push(
        {
          message: "Not a great artist",
          variant: "warning"
        },
        {
          message: "You submitted it",
          variant: "success"
        }
      );
    } else {
      new_status.push({
        message: "Bad Payee",
        variant: "danger"
      });
    }
    setStatuses(new_status);
  };

  return (
    <div className="lod-form">
      <Form onSubmit={handleSubmit}>
        <Form.Group controlId="payeeControl">
          <Form.Label>
            <b>Payee*</b>
          </Form.Label>
          <Form.Control
            as="select"
            onChange={evt => setPayee(evt.target.value)}
          >
            {options.map((cur, idx) => {
              return (
                <option key={idx} value={cur.value}>
                  {cur.text}
                </option>
              );
            })}
          </Form.Control>
        </Form.Group>
        <Form.Group controlId="artistNameControl">
          <Form.Label>
            <b>Artist Name*</b>
          </Form.Label>
          <Form.Control
            type="text"
            placeholder="Artist Name"
            value={artist}
            onChange={evt => setArtist(evt.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="artistLODFormControl">
          <Form.Label>
            <b>Artist LOD Form*</b>
          </Form.Label>
          <Form.Control
            type="file"
            placeholder="Artist LOD Form File Path"
            value={lodForm}
            onChange={evt => setLodForm(evt.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="artistLODRepControl">
          <Form.Label>
            <b>Artist LOD Repertoire Sheet*</b>
          </Form.Label>
          <Form.Control
            type="file"
            placeholder="Artist LOD Repertoire Sheet File Path"
            value={repSheet}
            onChange={evt => setRepSheet(evt.target.value)}
          />
        </Form.Group>
        {props.isAMP && (
          <Form.Group controlId="ampCertControl">
            <Form.Label>
              <b>AMP Act Certification*</b>
            </Form.Label>
            <Form.Control
              type="file"
              placeholder="AMP Act Certification File Path"
              value={ampCert}
              onChange={evt => setAmpCert(evt.target.value)}
            />
          </Form.Group>
        )}
        <Button type="submit" variant="primary">
          Submit
        </Button>
      </Form>
    </div>
  );
}

export default LODForm;
