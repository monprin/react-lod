import React, { useContext } from "react";
import { useHistory } from "react-router-dom";

import Alert from "react-bootstrap/Alert";

import LODContext from "../context/LODContext";

function Status(props) {
  const history = useHistory();
  const { setStatuses } = useContext(LODContext);

  history.listen(() => setStatuses([]));

  return (
    <div>
      {props.statuses.map((msg, idx) => {
        return (
          <Alert variant={msg.variant} key={idx}>
            {msg.message}
          </Alert>
        );
      })}
    </div>
  );
}

export default Status;
