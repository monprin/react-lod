import React, { useState } from "react";

import Button from "react-bootstrap/Button";
import ToggleButtonGroup from "react-bootstrap/ToggleButtonGroup";

import LODForm from "../components/LODUpload/LODForm";

function LODUpload() {
  // Declare a new state variable, which we'll call "count"
  const [isAMP, setIsAMP] = useState(false);

  return (
    <div className="lod-upload">
      <div>
        <h2>Letter of Direction (LOD)</h2>
        <p>
          SoundExchange accepts Letters-of-Direction (LOD) from directly
          registered featured artists who wish to divert a portion of their
          payment to a third party who was a creative participant on specific
          recordings. Typically SoundExchange will only honor an LOD which
          directs a percentage to a producer, mixer, or engineer.
        </p>
        <p>
          For your convenience, this section of SoundExchange Direct allows you
          to upload LODs related to specific registrant accounts that you have
          access to.
        </p>
        <ul>
          <li>
            Download and complete the Letter of Direction form and LOD
            Repertoire Sheet
          </li>
          <li>
            Remember an LOD is incomplete if it does not include both parts
          </li>
          <li>
            Looking for more information about Letters of Direction, check out
            the FAQ
          </li>
        </ul>
      </div>
      <div>
        <ToggleButtonGroup type="radio" name="lod-type" defaultValue="standard">
          <Button
            variant="secondary"
            value="standard"
            active={!isAMP}
            onClick={() => setIsAMP(false)}
          >
            Standard LOD
          </Button>
          <Button
            variant="secondary"
            value="amp"
            active={isAMP}
            onClick={() => setIsAMP(true)}
          >
            AMP Pre-1995
          </Button>
        </ToggleButtonGroup>
      </div>
      <br />
      {isAMP && (
        <div>
          <b>AMP Specific Information</b>
          <ul>
            <li>
              If you are submitting LOD(s) for pre-1995 recordings, check out
              our Guide to Pre-1995 Recordings Letters of Direction
            </li>
            <li>
              For pre-1995 AMP Default Allocation applications (who have not
              obtained an Artist signature or decline/objection after 120 days
              of reasonable attempts to contact them) download and complete the
              “Amp Act Certification” form. Note - this form must be notarized.
            </li>
            <li>Select the Registrant for which you wish to submit the LOD</li>
            <li>
              Upload the LOD, LOD Repertoire Sheet, Amp Act Certification form,
              and any pertinent contracts relating to the applicable recordings.
            </li>
          </ul>
        </div>
      )}
      <LODForm isAMP={isAMP} />
    </div>
  );
}

export default LODUpload;
