import React, { useState } from "react";
import styled from "styled-components";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink
} from "react-router-dom";

import { LODProvider } from "./context/LODContext";
import sx_logo from "./assets/sx_logo.png";
import Home from "./views/Home";
import Status from "./components/Status";
import LODUpload from "./views/LODUpload";

// Style
const MainContainer = styled.div`
  font-family: "Avenir", Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: #2c3e50;
  text-align: left;
  margin: 20px;
`;

const Header = styled.div`
  padding-bottom: 10px;
`;

const NavBar = styled.div`
  padding-bottom: 10px;
  font-size: 2em;
`;

const RealNavLink = styled(NavLink)`
  font-weight: bold;
  color: #2c3e50;

  &.active {
    color: #42b983;
  }
`;

const FakeNavLink = styled.b`
  color: #6c757d;
`;

// Component
function App() {
  const [statuses, setStatuses] = useState([]);

  return (
    <LODProvider value={{ setStatuses }}>
      <Router>
        <MainContainer>
          <Header>
            <img alt="SX logo" src={sx_logo} />
            <NavBar>
              <RealNavLink to="/">Home</RealNavLink>
              &nbsp;|&nbsp;
              <FakeNavLink>Settings</FakeNavLink>&nbsp;|&nbsp;
              <FakeNavLink>My Catalog</FakeNavLink>&nbsp;|&nbsp;
              <RealNavLink to="/lod">LOD</RealNavLink>
              &nbsp;|&nbsp;
              <FakeNavLink>Contact</FakeNavLink>&nbsp;|&nbsp;
              <FakeNavLink>FAQ</FakeNavLink>
            </NavBar>
            <Status statuses={statuses} />
            <Switch>
              <Route path="/lod">
                <LODUpload />
              </Route>
              <Route path="/">
                <Home />
              </Route>
            </Switch>
          </Header>
        </MainContainer>
      </Router>
    </LODProvider>
  );
}

export default App;
