import React from "react";

const LODContext = React.createContext({});

export const LODProvider = LODContext.Provider;
export const LODConsumer = LODContext.Consumer;
export default LODContext;
